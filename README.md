# server
This project is the back-end for Ta Rang Rian project.

## Project Setup (Windows)
```
pip install virtualenv
virtualenv venv
.\venv\Scripts\activate 
pip install -r requirements.txt
```

## Database Setup (Windows)
1. open sqlite3.exe
```
sqlite3.exe
```

2. create database
```
.open tarangrian.db
```

3. check database
```
.databases
```

4. create database model
```
python
from app import db
db.create_all()
```


## Compiles and hot-reloads
```
python app.py
```


## Customize configuration
- [Virtualenv](https://virtualenv.pypa.io/en/latest/index.html)
- [Flask](https://flask.palletsprojects.com/en/2.0.x/)
- [SQLite3](https://www.sqlite.org/index.html).
