from flask import Flask, json, request, jsonify, make_response, send_file
from flask_sqlalchemy import SQLAlchemy
from werkzeug.security import generate_password_hash, check_password_hash
from functools import wraps
from werkzeug.utils import send_file
from flask_cors import CORS, cross_origin
from dotenv import load_dotenv
import os
import uuid
import jwt
import datetime
import secrets
import base64

#App
app = Flask(__name__)

#Enable CORS
cors = CORS(app, resources={r"/*": {"origins": "*"}})

#env
load_dotenv()
BASE_URL = os.environ.get('BASE_URL')

#Config
app.config['SECRET_KEY'] = 'qi0g?G[y>fzm>xQ1|g|kZ)wnqy:,f:'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///D:\\Programming\\SQLite3\\tarangrian.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True

#Database Config
db = SQLAlchemy(app) 

#Database Model
class Users(db.Model):
    id = db.Column(db.String(8))
    public_id = db.Column(db.Integer)
    email = db.Column(db.String(50))
    username = db.Column(db.String(50), primary_key=True)
    password = db.Column(db.String(50))
    admin = db.Column(db.Boolean)

class Subjects(db.Model):
    id = db.Column(db.String(8), primary_key=True)
    code = db.Column(db.String(20), nullable=False)
    name = db.Column(db.String(50), nullable=False)
    type = db.Column(db.String(20), nullable=False)
    location = db.Column(db.String(50))
    teacher = db.Column(db.String(50))
    phone = db.Column(db.String(20))
    date = db.Column(db.String(20))
    timeStart = db.Column(db.String(10))
    timeEnd = db.Column(db.String(10))
    note = db.Column(db.String(50))
    color = db.Column(db.String(20))
    user_id = db.Column(db.String(16), nullable=False)

@app.errorhandler(404)
def page_not_found(e):
    return jsonify(error=str(e)), 404

#Check token
def token_required(f):
    @wraps(f)
    def decorator(*args, **kwargs):
        token = None

        if 'x-access-tokens' in request.headers:
            token = request.headers['x-access-tokens']
        
        if not token:
            return jsonify({'message': 'a valid token is missing'})
        
        try:
            data = jwt.decode(token, app.config['SECRET_KEY'], algorithms='HS256')
            current_user = Users.query.filter_by(public_id=data['public_id']).first()
        except:
            return jsonify({'message': 'token is invalid'})
        
        return f(current_user, *args, **kwargs)
    
    return decorator

#Routes
#Register
@app.route('/register', methods=['POST'])
def register_user():
    data = request.get_json()
    hashed_password = generate_password_hash(data['password'], method='sha256')
    new_id = secrets.token_hex(8)

    default_image = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAABAAAAAQAAgMAAAACc8MQAAAABGdBTUEAALGPC/xhBQAAAAFzUkdCAK7OHOkAAAAMUExURcXFxf///+np6dbW1sCaIHAAAAnDSURBVHja7d0xbuw2FEBRNW7UaGtu1LDR1tiwcTNbU6NmGgcJEqQIkPx4SOq9uffuQAcg+Uj/by+LmZmZmZmZmZmZmZmZmZmZmZmZmZmZmZmZmZmZmZmZmZmZmZmZmZmZmZmZmZmZmZmZmZmZmZmZmZmZmZmZmZmZmZmZmZmZmZmZmZmZmZmZmZmZmZmZmZmZmZmZmZmZ5e77+fW1/1H5+vo+BYB9/Z/f/nflcQpAafvH1/9lIAD58zEEcIDtuf9r5VuAt+7Z9v/q6xQAuf8hdkI6wHfbf613nYrpAM/9lyunAOjvf08BOMCvnYBvfBbSAba2/98eArC//70E8AA/+f59rwIgZ6B3nIboAD/8/vcRoAP8+PvfRYAOsO0vdAjAnILfaR7GA7z2/W8wD9MBni9+f/qTkA6w7S93CEDeAtNvg3SAZ4fvT70N0gG2vUuHAOQtMPU2SAd4dvr+tNsgHWDrtQKyPovgAbqtgKxrgA6w7R07BMjX1RNgPwVAr4CMawAP0PoC5LsQ0QHWzt+/FwHYKyDdGqADbN2/P9lBiAe4+gPkmobpAANWQK41gAcYsQJSrQE6wNaGADwEAD6I53wcxwOMWQGJ7kN0gHXQ96d5FMEDXKMAsoxCdIBt2PcnuQ/hAdZxAEUA8hiUZhSiAwzcA3PsgniAayRAhlEID9CGAlQBwGNQjlEIDzB2D0ywC+IB2mCAKgB4EM4wDOMBRu+B4XdBPEAbDlAFQO+B0XdBPMA6HqAIwD4Fg5+DeIA2AaAKgN4DY++CeIAZe2DoXRAP0KYAVAHAV6HY1yE8wDoHoAjAPgUDn4N4gDYJoAoggADgOSjuJIQHWGcBFAHYc1DYSQgP0KYBVAHQp2DUc1AAAeAA6zyAIgB7Dgo6CeEB2kSAKoAAAoDnoJiTEB5gnQlQBBBAAPIkHHIWFkAAOECbClAFCNfU798/BUDfhSLehvAA61yAIoAAAsTqYy7ALgD7LhTwNoQHaJMBqgACCAC+DQe8DwsgABtgmw1wCCCAAOD3kHgvIgIIAAeY/SIW7k1MAAHgANd0gFMAAQQQQADsq3i4d3EBBBBAAAEEEEAALMD074/201EBBBBAAAEEEEAAAQQQQAABBBBAAAEEEEAAAQQQQAAMgD8bFEAAAQQQQAABBKACXNMB/P8CAggggABx8v8OCyAAG8DfISKAAGwAf5eYAAKwAfydogIIAAfwd4sLIAAbYParqH9jJBqAf2eIDuDfGhNAADaAf3OUDuDfHcYD+LfH6QBzb0OnAAIIQJ6F403CAgiAB5h6GzoECNjMWbgKIIAA6Fn4FIA9CUWcgwSYOQkdAgggAHoSqgKE7GLPQQLMm4RizkECzDsHDwGC1tinoAAC4AEu9hwkwKxJKOocJMCsSegQIGyNfQoKMOkcPAVgn4NxT0EB5uyCVYCFvQueArB3wdB7oAATrkOHAKFr7FNQgAnn4CkAexcMvgcKMHwXrAIs7F3wFCB4g69DZRGAvQtWARb2LngKEL4NPQgLMHgXrAIkaEWPQQIM3QUPAVJ0kccgAUbugjn2QAHGjUJVgCQ9B62AU4AkbWPWwGMRIEsXeQwSYNR96FgEYK+BUwD2Gki1AgQYcB+qAqSq+6NIWQRgr4EqQLI29CEoQPdp+BSAvQYSrgABej6Ol1OAjGug3yz0WARAr4GcK0CAfheiugiQsw08BQnQbxtMuwUK0GkbrIsAC3kbPBYBMvdEb4ECdNgG6yJA7l58FnksAqQXAJ+BArx6EqY/AwV4TeBNvl+Anwq8zfcL8MN5uC4CvEfbD5fA4xTgPb6/wY9BOsDzhftweQhAvgq9x4MIHGD7evVZ/OsUgPsimv8spAP0+aeSiQXoAL3+rWxaATpAv38snVSADtDzd+mkFKAD9P1lSgkF6AC9f5tWOgE6QP9fp5ZMgA4w4vfppRLAA1z7gA4BKE/h+Z/I6QCDvj+PAB1g4B9YOAWADkGphiE8wNA/t1cE4J6BWU5COsDgv7ka/9/P0gG20X95O/pJiAe49uEdAnDPwPgnIR1g/BYYfBvEA0z5/sg3IjrAc5/UQwDwFhh4G8QDXPu0DgGITyHRn0XoAPO2wKDbIB7guU/tIUC0FbBP7hQAOwXFnIXoAOs+vSoA8DEs7sMYHeC539BDAOhFKOCFCA9wywqItAboAPesgEBrAA9w0wqIswboANt+W6cAyLeQaK8idIAbV0CMNYAHuO4EOARgr4AIawAPcN0LcAgAvQqHuRLjAZ77zT0EQK+A29cAHuD2FXD3GsADtPsBigA3tu4BqgKgV8C9a4AOEGIF3LkG8ABXDIBDAOZ76P0vo3iAZxSAhwDgMejOUYgOsO5hqgKgV8Bda4AOsO2BOgXA3oTuuw/hAVokgCIAeQy6ZxTCA1yxAA4ByGPQHaMQHmCNBlAFAI9Bd4xCdIBwe+DsXRAPcMUDOARA74Gzd0E6wLoHrAqA3gPn7oJ4gBYRoAjA3gNn7oJ4gCsmwCEAeg+cuQvSAYLugfN2QTzAFRXgEAC9B87bBekAYffAWbsgHuCKC3AIgN4DZ+2CdIBtD9wpAPoUnHMO4gFaZIAiAHsPnLEL4gHW2ABVAPBVaM51CA/QYgMUAdin4PhzEA+wRgeoAqD3wPG7IB0g/B44ehfEA6zxAaoA4KvQ+OsQHqDFBygCsE/BsecgHmDNAFAFQJ+CY89BPEDLAFAEYJ+CI89BPMCaA6AKgD4FR56DeICWA6AIwD4Fx52DeIA1C0AVAH0KjjsH8QAtC0ARQAAByHPQqEkID7DmAagCoOegUZMQHqDlASgCsE/BMecgHmDNBFAFEEAA8Bw0ZhLCA7RMAEUA9hw0YhLCA6y5AKoAAggAnoNGTEJ4gJYLoAjAnoP6T0ICCAAHWLMBVAHQc1D/SQgP0LIBFAEEEIA8B/WehPAAaz6AKoAAAoAn4d6zMB6g5QMoAgggAHkS7jsL4wHWjABVAAEEAE/CfWdhAQSAA7SMAEWAfu0pE4B9F+p5G8IDrDkBqgACCACehHvOwgIIAAdoOQGKAAIIQL4L9bsNCSAAHGDNClAFEEAA8F2o321IAAHgAC0rQBFAAAG4Pxz/PQHYt+Fe92EBBIADrHkBqgACCPB6H3kBPgVAv4f0ehERQAA4QMsLUAQQQADyi1inNzE6wJYZ4BRAAAHA7yF9XkQEEEAAAdAAH5kBPgVAv4j1eRMTQAABBEADtMwARQABBBBAAO7PRbr8ZEQAAdgAW26AUwABBBBAAOzPRXr8ZEQAAQQQQAABwAAfuQE+BRBAAAEEwP50vMfPxwUQQAABBBAADNByAxQBBBBAAAH+pd8AoDH47zbOd5EAAAAASUVORK5CYII="
    
    with open('./static/profile_image/'+new_id+'.png', 'wb') as fh:
        fh.write(base64.urlsafe_b64decode(default_image.split(',')[1]))

    new_user = Users(
        id = new_id,
        public_id = str(uuid.uuid4()),
        email = data['email'],
        username = data['username'],
        password = hashed_password,
        admin = False
    )

    try:
        db.session.add(new_user)
        db.session.commit()
        return jsonify({'message': 'registered successfully'})

    except:
        return jsonify({'message': 'register failed'})

#Login
@app.route('/login', methods=['GET', 'POST'])
def login_user():
    auth = request.authorization

    if not auth or not auth.username or not auth.password:
        return make_response('could not verify', 401, {'WWW.Authentication': 'Basic realm: "login required"'})

    try:
        user = Users.query.filter_by(email = auth.username).first()

        if check_password_hash(user.password, auth.password):
            token = jwt.encode({
                'public_id': user.public_id,
                'exp': datetime.datetime.utcnow() + datetime.timedelta(minutes=10080)},
                app.config['SECRET_KEY'],
                algorithm='HS256'
            )

            response = {
                'token': token,
                'user':  {'username': user.username, 'email': user.email}
            }

            return jsonify(response)
        return make_response('could not verify', 401, {'WWW.Authentication': 'Basic realm: "login required"'})
    
    except:
        return jsonify({'message':'login failed'})

#GET User
@app.route('/user', methods=['GET'])
@token_required
def get_user(current_user):
    user = Users.query.filter_by(id = current_user.id).first()

    response = {
        'user': {'username': user.username, 'email': user.email}
    }

    return make_response(jsonify(response), 200)

#GET Image
@app.route('/image', methods=['GET'])
@token_required
def get_image(current_user):
    image_url = BASE_URL+'/static/profile_image/'+current_user.id+'.png'

    return jsonify({'image': image_url})

#ADD Subject
@app.route('/subject', methods=['POST'])
@token_required
def create_subject(current_user):
    data = request.get_json()
    
    new_subject = Subjects(
        id = str(secrets.token_hex(4)),
        code = data['code'],
        name = data['name'],
        type = data['type'],
        location = data['location'],
        teacher = data['teacher'],
        phone = data['phone'],
        date = data['date'],
        timeStart = data['timeStart'],
        timeEnd = data['timeEnd'],
        note = data['note'],
        color = data['color'],
        user_id = current_user.id,
    )
    
    try:
        db.session.add(new_subject)
        db.session.commit()
        return jsonify({'message': 'new subject created'})

    except:
        return jsonify({'message': 'new subject create failed'})

#GET subjects
@app.route('/subjects', methods=['GET'])
@token_required
def get_subjects(current_user):
    subjects = Subjects.query.filter_by(user_id=current_user.id).all()
    output = []

    for subject in subjects:
        subject_data = {}
        subject_data['id'] = subject.id
        subject_data['code'] = subject.code
        subject_data['name'] = subject.name
        subject_data['type'] = subject.type
        subject_data['location'] = subject.location
        subject_data['teacher'] = subject.teacher
        subject_data['phone'] = subject.phone
        subject_data['date'] = subject.date
        subject_data['timeStart'] = subject.timeStart
        subject_data['timeEnd'] = subject.timeEnd
        subject_data['note'] = subject.note
        subject_data['color'] = subject.color

        output.append(subject_data)
    
    if len(output) == 0:
        return jsonify({'message':'no subject'})

    return jsonify({
        'message': 'get subjects successfully',
        'list_of_subjects': output,
    })

#GET subject
@app.route('/subject/<subject_id>', methods=['GET'])
@token_required
def get_subject(current_user, subject_id):
    subject = Subjects.query.filter_by(user_id=current_user.id, id=subject_id).first()

    subject_data = {}
    subject_data['id'] = subject.id
    subject_data['code'] = subject.code
    subject_data['name'] = subject.name
    subject_data['type'] = subject.type
    subject_data['location'] = subject.location
    subject_data['teacher'] = subject.teacher
    subject_data['phone'] = subject.phone
    subject_data['date'] = subject.date
    subject_data['timeStart'] = subject.timeStart
    subject_data['timeEnd'] = subject.timeEnd
    subject_data['note'] = subject.note
    subject_data['color'] = subject.color

    return jsonify({'subject':subject_data})

#UPDATE Subject
@app.route('/subject', methods=['PUT'])
@token_required
def update_subject(current_user):
    data = request.get_json()

    #Get sujbect => Change in temp => Delete subject => Add temp back

    try:
        subject = Subjects.query.filter_by(id = data['id'], user_id = current_user.id).first()

        subject_update = Subjects(
            id = subject.id,
            code = data['code'],
            name = data['name'],
            type = data['type'],
            location = data['location'],
            teacher = data['teacher'],
            phone = data['phone'],
            date = data['date'],
            timeStart = data['timeStart'],
            timeEnd = data['timeEnd'],
            note = data['note'],
            color = data['color'],
            user_id = current_user.id
        )

        db.session.delete(subject)
        db.session.add(subject_update)
        db.session.commit()
        return jsonify({'message': 'subject updated'})

    except:
        return jsonify({'message': 'subject does not exist'})

#DELETE Subject
@app.route('/subject/<subject_id>', methods=['DELETE'])
@token_required
def delete_subject(current_user, subject_id):
    data = request.get_json()

    try:
        subject = Subjects.query.filter_by(id = subject_id, user_id = current_user.id).first()

        db.session.delete(subject)
        db.session.commit()
        return jsonify({'message': 'subject deleted'})
    
    except:
        return jsonify({'message': 'subject does not exist'})

#UPDATE User
@app.route('/user', methods=['PUT'])
@token_required
def update_user(current_user):
    data = request.get_json()

    try:
        image = data['image']

        with open('./static/profile_image/'+current_user.id+'.png', 'wb') as fh:
            fh.write(base64.urlsafe_b64decode(image.split(',')[1]))

        user = Users.query.filter_by(id = current_user.id).first()

        user_update = Users(
            id = current_user.id,
            public_id = current_user.public_id,
            email = data['email'],
            username = data['username'],
            password = current_user.password,
            admin = False
        )

        db.session.delete(user)
        db.session.add(user_update)
        db.session.commit()
        return jsonify({'message': 'user updated'})

    except Exception as err:
        print(err)
        return jsonify({'message': 'edit user failed'})

#CHANGE Password
@app.route('/changepassword', methods=['PUT'])
@token_required
def change_password(current_user):
    data = request.get_json()

    try:
        if(check_password_hash(current_user.password, data['oldPassword'])):
            user = Users.query.filter_by(id = current_user.id).first()

            new_password = generate_password_hash(data['newPassword'], method='sha256')

            user_new_password = Users(
                    id = current_user.id,
                    public_id = current_user.public_id,
                    email = current_user.email,
                    username = current_user.username,
                    password = new_password,
                    admin = False
            )

            db.session.delete(user)
            db.session.add(user_new_password)
            db.session.commit()

            return jsonify({'message': 'change password successfully'})

        else:
            return jsonify({'message': 'current password is not correct'})
    
    except:
        return jsonify({'message': 'change password failded'})

#init
if __name__ == '__main__':
    app.debug = True
    app.run()

